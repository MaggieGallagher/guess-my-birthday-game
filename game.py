from random import randint

name = input("Hi What is your name?")

for i in range(1, 6):
    month_guess = randint(1, 12)
    year_guess = randint (1924, 2004)

    print(f"Guess {i}: {name} were you born in {month_guess} / {year_guess}?")

    response = input("yes or no")

    if response == " yes":
        print("I knew it!")
        break
    elif response == " no" and i <5:
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")
